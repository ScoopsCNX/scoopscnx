//
//  FontUntil.swift
//  ScoopsCNX
//
//  Created by JMY on 14/12/2561 BE.
//  Copyright © 2561 JMY. All rights reserved.
//

import Foundation
import UIKit

class FontUntil {
    enum FontType {
        case regular, bold, heavy
        
        var description: String {
            switch self {
                case .regular:
                    return R.font.sfProDisplayRegular.fontName
                case .bold:
                    return R.font.sfProDisplayBold.fontName
                case .heavy:
                    return R.font.sfProDisplayHeavy.fontName
            }
        }
    }
    
    enum FontColor {
        case orange, brown, navColor
        
        var colorData: UIColor {
            switch self {
                case .orange:
                    return UIColor(hexFromString: "#D87F2E")
                case .brown:
                    return UIColor(hexFromString: "#4A4A4A")
                case .navColor:
                    return UIColor(hexFromString: "#F4960D")
            }
        }
    }
    
    func setFontLabelArray(label: [UILabel], fontType: FontType, fontSize: CGFloat) {
        label.forEach{ $0.font = UIFont(name: fontType.description, size: fontSize)}
    }
    
    func setFontLabel(label: UILabel, fontType: FontType, fontSize: CGFloat) {
        label.font = UIFont(name: fontType.description, size: fontSize)
    }
    
    func setColorLabelArray(label: [UILabel], color: FontColor) {
        label.forEach{ $0.textColor = color.colorData }
    }
    
    func setColorLabel(label: UILabel, color: FontColor) {
        label.textColor = color.colorData
    }
    
}

extension UIColor {
    convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt32 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
