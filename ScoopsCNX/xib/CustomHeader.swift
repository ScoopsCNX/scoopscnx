//
//  CustomHeader.swift
//  ScoopsCNX
//
//  Created by JMY on 14/12/2561 BE.
//  Copyright © 2561 JMY. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CustomHeader: UITableViewHeaderFooterView {

    var disposeBag = DisposeBag()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    @IBOutlet weak var titleBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        settingFont()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    private func settingFont() {
        FontUntil().setFontLabel(label: titleLabel, fontType: .bold, fontSize: 15.0)
        FontUntil().setColorLabel(label: titleLabel, color: FontUntil.FontColor.brown)
    }
    
}
