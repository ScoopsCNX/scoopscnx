//
//  ContactTableViewCell.swift
//  ScoopsCNX
//
//  Created by JMY on 15/12/2561 BE.
//  Copyright © 2561 JMY. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    //telephon
    @IBOutlet weak var telephonTitleLabel: UILabel!
    @IBOutlet weak var telephonDataLabel: UILabel!
    
    //mobile
    @IBOutlet weak var mobileTitleLabel: UILabel!
    @IBOutlet weak var mobileDataLabel: UILabel!
    
    //email
    @IBOutlet weak var emailTitleLabel: UILabel!
    @IBOutlet weak var emailDataLabel: UILabel!
    
    //address
    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var addressDataLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initSetting()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func initSetting() {
        FontUntil().setFontLabelArray(label: [telephonTitleLabel, mobileTitleLabel, emailTitleLabel, addressTitleLabel], fontType: .bold, fontSize: 15.0)
        
        FontUntil().setFontLabelArray(label: [telephonDataLabel, mobileDataLabel, emailDataLabel, addressDataLabel], fontType: .regular, fontSize: 15.0)
        
        FontUntil().setColorLabelArray(label: [telephonTitleLabel, mobileTitleLabel, emailTitleLabel, addressTitleLabel,
                                               telephonDataLabel, mobileDataLabel, addressDataLabel],
                                       color: .brown)
        
        FontUntil().setColorLabel(label: emailDataLabel, color: .orange)
        
        telephonTitleLabel.text = "TELEFON BüRO"
        mobileTitleLabel.text = "HANDY"
        emailTitleLabel.text = "EMAIL"
        addressTitleLabel.text = "ADDRESS"
        
    }
    
    func setData(_ user: UserModel) {
        telephonDataLabel.text = user.contact.telephone
        mobileDataLabel.text = user.contact.mobile
        emailDataLabel.text = user.contact.email
        addressDataLabel.text = user.contact.address
    }
    
}
