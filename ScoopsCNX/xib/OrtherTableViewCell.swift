//
//  OrtherTableViewCell.swift
//  ScoopsCNX
//
//  Created by JMY on 15/12/2561 BE.
//  Copyright © 2561 JMY. All rights reserved.
//

import UIKit

class OrtherTableViewCell: UITableViewCell {

//    outlineData
    @IBOutlet weak var outlineTitleLabel: UILabel!
    @IBOutlet weak var outlineDataLabel: UILabel!
    
//    history
    @IBOutlet weak var historyTitleLabel: UILabel!
    @IBOutlet weak var historyDataLabel: UILabel!
    
//    attention
    @IBOutlet weak var attentionTitleLabel: UILabel!
    @IBOutlet weak var attentionDataLabel: UILabel!
    
//    expertise
    @IBOutlet weak var expertiseTitleLabel: UILabel!
    @IBOutlet weak var expertiseDataLabel: UILabel!
    
//    discipline
    @IBOutlet weak var disciplineTitleLabel: UILabel!
    @IBOutlet weak var disciplineDataLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initSetting()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func initSetting() {
        FontUntil().setFontLabelArray(label: [outlineTitleLabel, historyTitleLabel, attentionTitleLabel,
                                              expertiseTitleLabel, disciplineTitleLabel], fontType: .bold, fontSize: 15.0)
        
        FontUntil().setFontLabelArray(label: [outlineDataLabel, historyDataLabel, attentionDataLabel,
                                              expertiseDataLabel, disciplineDataLabel], fontType: .regular, fontSize: 15.0)

        FontUntil().setColorLabelArray(label: [outlineTitleLabel, historyTitleLabel, attentionTitleLabel,
                                               expertiseTitleLabel, disciplineTitleLabel, outlineDataLabel,
                                               historyDataLabel, attentionDataLabel,expertiseDataLabel,
                                               disciplineDataLabel],
                                       color: .brown)
        
        outlineTitleLabel.text = "Rahmendaten".uppercased()
        historyTitleLabel.text = "Historie".uppercased()
        attentionTitleLabel.text = "Schwerpunkt".uppercased()
        expertiseTitleLabel.text = "Persönliche Expertise".uppercased()
        disciplineTitleLabel.text = "Persönliches Fachgebiet".uppercased()

    }
    
    func setData(_ user: UserModel) {
        outlineDataLabel.text = user.orther.outlineData
        historyDataLabel.text = user.orther.history
        attentionDataLabel.text = user.orther.attention
        expertiseDataLabel.text = user.orther.expertise
        disciplineDataLabel.text = user.orther.discipline
    }
    
}
