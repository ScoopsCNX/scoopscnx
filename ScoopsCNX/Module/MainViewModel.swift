//
//  MainViewModel.swift
//  ScoopsCNX
//
//  Created by JMY on 14/12/2561 BE.
//  Copyright © 2561 JMY. All rights reserved.
//

import Foundation

struct MainUserData {
    var data = UserModel()
    var isExpandedContact = false
    var isExpandedOrther = false
}

protocol MainViewModelInput {
    func onTapContact()
    func onTapOrther()
}

protocol MainViewModelOutput {
    var data: MainUserData { get }
}

protocol MainViewModelType {
    var inputs: MainViewModelInput { get }
    var outputs: MainViewModelOutput { get }
}

class MainViewModel: MainViewModelInput, MainViewModelOutput, MainViewModelType  {
    var inputs: MainViewModelInput { return self }
    var outputs: MainViewModelOutput { return self }
    
    var data: MainUserData = MainUserData()
    
    func onTapContact(){
        data.isExpandedContact.toggle()
    }
    
    func onTapOrther() {
        data.isExpandedOrther.toggle()
    }
}
