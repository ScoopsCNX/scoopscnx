//
//  MainViewController.swift
//  ScoopsCNX
//
//  Created by JMY on 14/12/2561 BE.
//  Copyright © 2561 JMY. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView
import RxCocoa
import RxSwift
import SnapKit

class MainViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    //Unternehmen
    @IBOutlet weak var unternehmenTitleLabel: UILabel!
    @IBOutlet weak var unternehmenDataLabel: UILabel!
    
    //Position
    @IBOutlet weak var positionTitleLabel: UILabel!
    @IBOutlet weak var positionDataLabel: UILabel!
    
    //Branche
    @IBOutlet weak var brancheTitleLabel: UILabel!
    @IBOutlet weak var brancheDataLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        settingFont()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    private func settingFont() {
        FontUntil().setFontLabel(label: titleLabel, fontType: .heavy, fontSize: 26.0)
        FontUntil().setFontLabelArray(label: [unternehmenTitleLabel, positionTitleLabel, brancheTitleLabel], fontType: .bold, fontSize: 15.0)
        FontUntil().setFontLabelArray(label: [unternehmenDataLabel, positionDataLabel, brancheDataLabel], fontType: .regular, fontSize: 15.0)
        
        FontUntil().setColorLabelArray(label: [titleLabel, unternehmenTitleLabel, unternehmenDataLabel,
                                               positionTitleLabel, positionDataLabel, brancheTitleLabel,
                                               brancheDataLabel], color: .brown)
        
        unternehmenTitleLabel.text = "UNTERNEHMEN"
        positionTitleLabel.text = "POSITION"
        brancheTitleLabel.text = "BRANCHE"
        
    }
    
    func setData(_ user: UserModel) {
        titleLabel.text = user.name
        unternehmenDataLabel.text = user.company
        positionDataLabel.text = user.position
        brancheDataLabel.text = user.branch
    }
}

class MainViewController: UIViewController {

    var stretchyHeader: GSKStretchyHeaderView!
    lazy var imageView =  UIImageView()
    
    @IBOutlet weak var tableView: UITableView!
    lazy var viewModel: MainViewModelType = {
        return MainViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initHeader()
        initTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = FontUntil.FontColor.navColor.colorData
    }
    
    func initHeader() {
        let headerSize = CGSize(width: tableView.frame.size.width,
                                height: 150) // 200 will be the default height

        stretchyHeader = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0,
                                                                          width: headerSize.width,
                                                                          height: headerSize.height))
        
        imageView.image = R.image.imageUser()
        imageView.contentMode = .scaleAspectFill
        
        stretchyHeader.minimumContentHeight = 0
        stretchyHeader.maximumContentHeight = 300
        stretchyHeader.expansionMode = .topOnly

        stretchyHeader.contentView.addSubview(imageView)
        stretchyHeader.stretchDelegate = self
        
        tableView.addSubview(self.stretchyHeader)
        
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(stretchyHeader)
            make.leading.equalTo(stretchyHeader)
            make.trailing.equalTo(stretchyHeader)
            make.bottom.equalTo(stretchyHeader)
        }
    }
    
    
    
    func initTable() {
        //register header
        tableView.registerHeaderFooterView(R.nib.customHeader)
        //register cell
        tableView.register(R.nib.contactTableViewCell)
        tableView.register(R.nib.ortherTableViewCell)
        
        tableView.estimatedRowHeight = 500
        tableView.estimatedSectionHeaderHeight = 100
        tableView.dataSource = self
        tableView.delegate = self
    }
    
}

extension MainViewController: GSKStretchyHeaderViewStretchDelegate {
    
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        
    }
    
    
}

extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let userData = viewModel.outputs.data
        if section == 0 {
            return 1
        } else if section == 1 {
            return userData.isExpandedContact ? 1 : 0
        } else {
            return userData.isExpandedOrther ? 1 : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellName = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.mainViewTableViewCell, for: indexPath) else {
            fatalError("Can't dequeueReusableCell mainViewTableViewCell")
        }
        guard let cellContact = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.contactTableViewCell, for: indexPath) else {
            fatalError("Can't dequeueReusableCell contactTableViewCell")
        }
        guard let cellOrther = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.ortherTableViewCell, for: indexPath) else {
            fatalError("Can't dequeueReusableCell ortherTableViewCell")
        }
        
        cellName.selectionStyle = .none
        cellContact.selectionStyle = .none
        cellOrther.selectionStyle = .none
        
        let userData = viewModel.outputs.data.data
        
        if indexPath.section == 1 {
            cellContact.setData(userData)
            return cellContact
        } else if indexPath.section == 2 {
            cellOrther.setData(userData)
            return cellOrther
        }
        
        cellName.setData(userData)
        
        return cellName
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let cellHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.reuseIdentifier.customHeader.identifier) as? CustomHeader else {
            fatalError("Can't dequeueReusableCell customHeader")
            
        }
        
        let userData = viewModel.outputs.data
        
        if section == 1 {
            cellHeader.titleLabel.text = "Contact Information"
            cellHeader.arrowImg.image = userData.isExpandedContact ? R.image.arrowUp() : R.image.arrowDown()
            cellHeader.titleBtn.rx.tap
                .asDriver()
                .drive(onNext: { [weak self] (_) in
                    guard let `self` = self else { return }
                    self.viewModel.inputs.onTapContact()
                    tableView.reloadSections(IndexSet(integer: section), with: .automatic)
            }).disposed(by: cellHeader.disposeBag)
        } else {
            cellHeader.titleLabel.text = "Other Information"
            cellHeader.arrowImg.image = userData.isExpandedOrther ? R.image.arrowUp() : R.image.arrowDown()
            cellHeader.titleBtn.rx.tap
                .asDriver()
                .drive(onNext: { [weak self] (_) in
                    guard let `self` = self else { return }
                    self.viewModel.inputs.onTapOrther()
                    tableView.reloadSections(IndexSet(integer: section), with: .automatic)
                }).disposed(by: cellHeader.disposeBag)
        }
        
        return cellHeader
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.00001
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }

    
}

extension MainViewController: UITableViewDelegate {
    
}
