//
//  UserModel.swift
//  ScoopsCNX
//
//  Created by JMY on 14/12/2561 BE.
//  Copyright © 2561 JMY. All rights reserved.
//

import Foundation

class UserModel: NSObject {
    var name: String = "Florian Bruckmann"
    var company: String = "Teigfabrik Edenhausen Betriebs GmbH"
    var position: String = "Geschäftsführender Gesellschafter"
    var branch: String = "Geschäftsführender Gesellschafter"
    var contact = ContactUser()
    var orther = OrtherUser()
}

class ContactUser: NSObject {
    var telephone: String = "0172-4376894"
    var mobile: String = "0172-4376894"
    var email: String = "jonwatkins@gmail.com"
    var address: String = "123/4, Several-Fleestedt Road, Hamburg, Germany 20999"
}

class OrtherUser: NSObject {
    var outlineData: String = "Sie sparen sich das Gären und das Formen des Teiges. Der Pizzaboden muss nur noch belegt und gebacken werden, dies kann auch von ungelernten Mitarbeitern problemlos bewältigt."
    var history: String = "Der Pizzaboden muss nur noch belegt und gebacken werden, dies kann auch von ungelernten Mitarbeitern problemlos bewältigt."
    var attention: String = "Der Pizzaboden muss nur noch belegt und gebacken werden, dies kann auch von ungelernten Mitarbeitern problemlos bewältigt."
    var expertise: String = "Der Pizzaboden muss nur noch belegt und gebacken werden, dies kann auch von ungelernten Mitarbeitern problemlos bewältigt."
    var discipline: String = "Der Pizzaboden muss nur noch belegt und gebacken werden, dies kann auch von ungelernten Mitarbeitern problemlos."
}
