//
// This is a generated file, do not edit!
// Generated by R.swift, see https://github.com/mac-cain13/R.swift
//

import Foundation
import Rswift
import UIKit

/// This `R` struct is generated and contains references to static resources.
struct R: Rswift.Validatable {
  fileprivate static let applicationLocale = hostingBundle.preferredLocalizations.first.flatMap(Locale.init) ?? Locale.current
  fileprivate static let hostingBundle = Bundle(for: R.Class.self)
  
  static func validate() throws {
    try font.validate()
    try intern.validate()
  }
  
  /// This `R.color` struct is generated, and contains static references to 0 colors.
  struct color {
    fileprivate init() {}
  }
  
  /// This `R.file` struct is generated, and contains static references to 3 files.
  struct file {
    /// Resource file `SF-Pro-Display-Bold.otf`.
    static let sfProDisplayBoldOtf = Rswift.FileResource(bundle: R.hostingBundle, name: "SF-Pro-Display-Bold", pathExtension: "otf")
    /// Resource file `SF-Pro-Display-Heavy.otf`.
    static let sfProDisplayHeavyOtf = Rswift.FileResource(bundle: R.hostingBundle, name: "SF-Pro-Display-Heavy", pathExtension: "otf")
    /// Resource file `SF-Pro-Display-Regular.otf`.
    static let sfProDisplayRegularOtf = Rswift.FileResource(bundle: R.hostingBundle, name: "SF-Pro-Display-Regular", pathExtension: "otf")
    
    /// `bundle.url(forResource: "SF-Pro-Display-Bold", withExtension: "otf")`
    static func sfProDisplayBoldOtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.sfProDisplayBoldOtf
      return fileResource.bundle.url(forResource: fileResource)
    }
    
    /// `bundle.url(forResource: "SF-Pro-Display-Heavy", withExtension: "otf")`
    static func sfProDisplayHeavyOtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.sfProDisplayHeavyOtf
      return fileResource.bundle.url(forResource: fileResource)
    }
    
    /// `bundle.url(forResource: "SF-Pro-Display-Regular", withExtension: "otf")`
    static func sfProDisplayRegularOtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.sfProDisplayRegularOtf
      return fileResource.bundle.url(forResource: fileResource)
    }
    
    fileprivate init() {}
  }
  
  /// This `R.font` struct is generated, and contains static references to 3 fonts.
  struct font: Rswift.Validatable {
    /// Font `SFProDisplay-Bold`.
    static let sfProDisplayBold = Rswift.FontResource(fontName: "SFProDisplay-Bold")
    /// Font `SFProDisplay-Heavy`.
    static let sfProDisplayHeavy = Rswift.FontResource(fontName: "SFProDisplay-Heavy")
    /// Font `SFProDisplay-Regular`.
    static let sfProDisplayRegular = Rswift.FontResource(fontName: "SFProDisplay-Regular")
    
    /// `UIFont(name: "SFProDisplay-Bold", size: ...)`
    static func sfProDisplayBold(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: sfProDisplayBold, size: size)
    }
    
    /// `UIFont(name: "SFProDisplay-Heavy", size: ...)`
    static func sfProDisplayHeavy(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: sfProDisplayHeavy, size: size)
    }
    
    /// `UIFont(name: "SFProDisplay-Regular", size: ...)`
    static func sfProDisplayRegular(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: sfProDisplayRegular, size: size)
    }
    
    static func validate() throws {
      if R.font.sfProDisplayBold(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'SFProDisplay-Bold' could not be loaded, is 'SF-Pro-Display-Bold.otf' added to the UIAppFonts array in this targets Info.plist?") }
      if R.font.sfProDisplayHeavy(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'SFProDisplay-Heavy' could not be loaded, is 'SF-Pro-Display-Heavy.otf' added to the UIAppFonts array in this targets Info.plist?") }
      if R.font.sfProDisplayRegular(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'SFProDisplay-Regular' could not be loaded, is 'SF-Pro-Display-Regular.otf' added to the UIAppFonts array in this targets Info.plist?") }
    }
    
    fileprivate init() {}
  }
  
  /// This `R.image` struct is generated, and contains static references to 3 images.
  struct image {
    /// Image `ArrowDown`.
    static let arrowDown = Rswift.ImageResource(bundle: R.hostingBundle, name: "ArrowDown")
    /// Image `ArrowUp`.
    static let arrowUp = Rswift.ImageResource(bundle: R.hostingBundle, name: "ArrowUp")
    /// Image `ImageUser`.
    static let imageUser = Rswift.ImageResource(bundle: R.hostingBundle, name: "ImageUser")
    
    /// `UIImage(named: "ArrowDown", bundle: ..., traitCollection: ...)`
    static func arrowDown(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.arrowDown, compatibleWith: traitCollection)
    }
    
    /// `UIImage(named: "ArrowUp", bundle: ..., traitCollection: ...)`
    static func arrowUp(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.arrowUp, compatibleWith: traitCollection)
    }
    
    /// `UIImage(named: "ImageUser", bundle: ..., traitCollection: ...)`
    static func imageUser(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.imageUser, compatibleWith: traitCollection)
    }
    
    fileprivate init() {}
  }
  
  /// This `R.nib` struct is generated, and contains static references to 3 nibs.
  struct nib {
    /// Nib `ContactTableViewCell`.
    static let contactTableViewCell = _R.nib._ContactTableViewCell()
    /// Nib `CustomHeader`.
    static let customHeader = _R.nib._CustomHeader()
    /// Nib `OrtherTableViewCell`.
    static let ortherTableViewCell = _R.nib._OrtherTableViewCell()
    
    /// `UINib(name: "ContactTableViewCell", in: bundle)`
    static func contactTableViewCell(_: Void = ()) -> UIKit.UINib {
      return UIKit.UINib(resource: R.nib.contactTableViewCell)
    }
    
    /// `UINib(name: "CustomHeader", in: bundle)`
    static func customHeader(_: Void = ()) -> UIKit.UINib {
      return UIKit.UINib(resource: R.nib.customHeader)
    }
    
    /// `UINib(name: "OrtherTableViewCell", in: bundle)`
    static func ortherTableViewCell(_: Void = ()) -> UIKit.UINib {
      return UIKit.UINib(resource: R.nib.ortherTableViewCell)
    }
    
    fileprivate init() {}
  }
  
  /// This `R.reuseIdentifier` struct is generated, and contains static references to 4 reuse identifiers.
  struct reuseIdentifier {
    /// Reuse identifier `ContactTableViewCell`.
    static let contactTableViewCell: Rswift.ReuseIdentifier<ContactTableViewCell> = Rswift.ReuseIdentifier(identifier: "ContactTableViewCell")
    /// Reuse identifier `CustomHeader`.
    static let customHeader: Rswift.ReuseIdentifier<CustomHeader> = Rswift.ReuseIdentifier(identifier: "CustomHeader")
    /// Reuse identifier `MainViewTableViewCell`.
    static let mainViewTableViewCell: Rswift.ReuseIdentifier<MainViewTableViewCell> = Rswift.ReuseIdentifier(identifier: "MainViewTableViewCell")
    /// Reuse identifier `OrtherTableViewCell`.
    static let ortherTableViewCell: Rswift.ReuseIdentifier<OrtherTableViewCell> = Rswift.ReuseIdentifier(identifier: "OrtherTableViewCell")
    
    fileprivate init() {}
  }
  
  /// This `R.segue` struct is generated, and contains static references to 0 view controllers.
  struct segue {
    fileprivate init() {}
  }
  
  /// This `R.storyboard` struct is generated, and contains static references to 2 storyboards.
  struct storyboard {
    /// Storyboard `LaunchScreen`.
    static let launchScreen = _R.storyboard.launchScreen()
    /// Storyboard `MainView`.
    static let mainView = _R.storyboard.mainView()
    
    /// `UIStoryboard(name: "LaunchScreen", bundle: ...)`
    static func launchScreen(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.launchScreen)
    }
    
    /// `UIStoryboard(name: "MainView", bundle: ...)`
    static func mainView(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.mainView)
    }
    
    fileprivate init() {}
  }
  
  /// This `R.string` struct is generated, and contains static references to 0 localization tables.
  struct string {
    fileprivate init() {}
  }
  
  fileprivate struct intern: Rswift.Validatable {
    fileprivate static func validate() throws {
      // There are no resources to validate
    }
    
    fileprivate init() {}
  }
  
  fileprivate class Class {}
  
  fileprivate init() {}
}

struct _R {
  struct nib {
    struct _ContactTableViewCell: Rswift.NibResourceType, Rswift.ReuseIdentifierType {
      typealias ReusableType = ContactTableViewCell
      
      let bundle = R.hostingBundle
      let identifier = "ContactTableViewCell"
      let name = "ContactTableViewCell"
      
      func firstView(owner ownerOrNil: AnyObject?, options optionsOrNil: [NSObject : AnyObject]? = nil) -> ContactTableViewCell? {
        return instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? ContactTableViewCell
      }
      
      fileprivate init() {}
    }
    
    struct _CustomHeader: Rswift.NibResourceType, Rswift.ReuseIdentifierType {
      typealias ReusableType = CustomHeader
      
      let bundle = R.hostingBundle
      let identifier = "CustomHeader"
      let name = "CustomHeader"
      
      func firstView(owner ownerOrNil: AnyObject?, options optionsOrNil: [NSObject : AnyObject]? = nil) -> CustomHeader? {
        return instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? CustomHeader
      }
      
      fileprivate init() {}
    }
    
    struct _OrtherTableViewCell: Rswift.NibResourceType, Rswift.ReuseIdentifierType {
      typealias ReusableType = OrtherTableViewCell
      
      let bundle = R.hostingBundle
      let identifier = "OrtherTableViewCell"
      let name = "OrtherTableViewCell"
      
      func firstView(owner ownerOrNil: AnyObject?, options optionsOrNil: [NSObject : AnyObject]? = nil) -> OrtherTableViewCell? {
        return instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? OrtherTableViewCell
      }
      
      fileprivate init() {}
    }
    
    fileprivate init() {}
  }
  
  struct storyboard {
    struct launchScreen: Rswift.StoryboardResourceWithInitialControllerType {
      typealias InitialController = UIKit.UIViewController
      
      let bundle = R.hostingBundle
      let name = "LaunchScreen"
      
      fileprivate init() {}
    }
    
    struct mainView: Rswift.StoryboardResourceWithInitialControllerType {
      typealias InitialController = UIKit.UINavigationController
      
      let bundle = R.hostingBundle
      let name = "MainView"
      
      fileprivate init() {}
    }
    
    fileprivate init() {}
  }
  
  fileprivate init() {}
}
